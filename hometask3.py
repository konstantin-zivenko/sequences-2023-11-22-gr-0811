# input --> str
# output --> {symbol(str): fequency(float)}

string = "Мама мила раму"

# result = {
#     "M": 1 / len(string),
# }

result = {}

# for symbol in string:
#     if symbol not in result:
#         result[symbol] = 1
#     else:
#         result[symbol] += 1
#
# number_symbols = len(string)
# for key in result.keys():
#     result[key] = result[key] / number_symbols


for symbol in string:
    result[symbol] = result.setdefault(symbol, 0) + 1

number_symbols = len(string)
for key in result.keys():
    result[key] = result[key] / number_symbols

print(result)
