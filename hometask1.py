# Створіть список та введіть в ного значення. Знайдіть найбільший та
# найменший елемент списку, а також суму та середнє арифметичне значень.

num_list = []

while True:
    num_str = input("введіть число (enter - кінець вводу): ")

    if not num_str:
        break
    num_list.append(float(num_str))
if len(num_list) != 0:
    print(
        f"max: {max(num_list)}",
        f"min: {min(num_list)}",
        f"average: {sum(num_list)/ len(num_list)}",
        sep="\n",
    )
print("len == 0")
